package com.mexmum.casbot;

import com.mexmum.casbot.services.ColorRoleCommand;
import com.mexmum.casbot.services.Command;
import com.mexmum.casbot.services.RoleAddCommand;
import com.mexmum.casbot.services.RoleCommand;
import com.mexmum.casbot.services.RoleRemoveCommand;
import net.dv8tion.jda.core.entities.Role;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.function.Consumer;


/**
 * Class that writes commands to a file, and read them from that same file.
 */
@SuppressWarnings("unchecked")
public final class CommandReadWriter {
    private static final String SERVICES_PACKAGE = "com.mexmum.casbot.services.";
    private static JSONArray commandList = new JSONArray();
    private static final Consumer<JSONObject> JSON_OBJECT_TO_COMMAND_CONSUMER = (jsonCommand) -> {
        Command command;
        String type = (String) jsonCommand.get("type");
        String key = (String) jsonCommand.get("key");
        String message = (String) jsonCommand.get("message");
        String roleId = (String) jsonCommand.getOrDefault("role", null);
        if (type.contains("Command")) {
            if (type.contains("Role")) {
                Role role = Main.getBot().getRoleById(roleId);
                if (role == null) {
                    throw new IllegalArgumentException(String.format("Role not found! On Command name: %1$s, Role id: %2$s", key, roleId));
                }
                if (type.contains("Add")) {
                    command = new RoleAddCommand(key, message, role);
                }
                else if (type.contains("Remove")) {
                    command = new RoleRemoveCommand(key, message, role);
                }
                else if (type.contains("Color")) {
                    command = new ColorRoleCommand(key, message, role);
                }
                else {
                    throw new IllegalArgumentException("Wrong type in file!");
                }
            }
            else {
                command = new Command(key, message);
            }
        }
        else {
            throw new IllegalArgumentException("Wrong type in file!");
        }
        Main.getRepository().addCommandToMap(command);
    };

    /**
     * Constructor that makes sure that no objects of this class can be made.
     */
    private CommandReadWriter() {
        throw new UnsupportedOperationException();
    }


    /**
     * Adds a command to the command file.
     * @param command the to be added command
     * @throws IllegalArgumentException thrown when the argument passed isn't an instance of the command class or one of it's child classes
     */
    public static void addCommand(Command command) throws IllegalArgumentException {
        String type = command.getClass().getName().substring(SERVICES_PACKAGE.length());
        JSONObject jsonCommand;

        switch (type) {
            case "RoleAddCommand":
            case "RoleRemoveCommand":
            case "ColorRoleCommand":
                jsonCommand = addRoleCommand((RoleCommand) command);
                break;

            case "Command":
                jsonCommand = addDefaultCommand(command);
                break;
            default:
                throw new IllegalArgumentException("Argument is not a command!");
        }

        commandList.add(jsonCommand);
        writeToFile(Main.getCommandFile());
        Main.getRepository().addCommandToMap(command);
    }

    /**
     * Converts a normal {@link com.mexmum.casbot.services.Command Command} to JSON format.
     * @param command the command
     * @return a {@link org.json.simple.JSONObject JSONObject} containing the data of the command and it's type
     */
    private static JSONObject addDefaultCommand(Command command) {
        JSONObject commandDetails = new JSONObject();
        commandDetails.put("type", command.getClass().getName().substring(SERVICES_PACKAGE.length()));
        commandDetails.put("key", command.getKey());
        commandDetails.put("message", command.getMessage());

        return commandDetails;
    }

    /**
     * Converts a {@link com.mexmum.casbot.services.RoleCommand RoleCommand} to JSON format.
     * @param command the command
     * @return a {@link org.json.simple.JSONObject JSONObject} containing the data of the command and it's type
     */
    private static JSONObject addRoleCommand(RoleCommand command) {
        JSONObject commandDetails = new JSONObject();
        commandDetails.put("type", command.getClass().getName().substring(SERVICES_PACKAGE.length()));
        commandDetails.put("key", command.getKey());
        commandDetails.put("message", command.getMessage());
        commandDetails.put("role", command.getRole().getId());

        return commandDetails;
    }

    /**
     * Writes the contents of the commandList to the given file.
     * @param file the file to which the commands are written
     */
    static void writeToFile(File file) {
        try {
            FileWriter writer = new FileWriter(file, false);
            writer.write(commandList.toJSONString());
            writer.flush();
            writer.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Reads the contents of the given file into the commandList.
     * @param file the file from which to read
     */
    static void readFromFile(File file) {
        //Empty commandList
        commandList.clear();

        try (FileReader reader = new FileReader(file)) {
            //Read JSON File
            JSONParser parser = new JSONParser();
            commandList = (JSONArray) parser.parse(reader);
        }  catch (IOException | ParseException e) {
            e.printStackTrace();
        }

        commandList.forEach(JSON_OBJECT_TO_COMMAND_CONSUMER);
    }
}
