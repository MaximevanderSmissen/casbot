package com.mexmum.casbot.services;

import com.mexmum.casbot.Main;
import com.mexmum.casbot.database.XPDatabaseConnector;
import com.mexmum.casbot.database.objects.UserXP;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;
import org.json.simple.JSONObject;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Handler that adds XPListeners to the bot when needed.
 */
public class XPListenerHandler extends ListenerAdapter {
    private ArrayList<String> userIdList;
    private final XPDatabaseConnector connector;

    /**
     * Default constructor for the XPListenerHandler.
     * @throws SQLException If the database isn't found or accessible
     */
    public XPListenerHandler() throws SQLException {
        this.userIdList = new ArrayList<>();
        JSONObject xpDatabaseLogin = (JSONObject) Main.getPropertiesObject().get("XPDatabase");
        this.connector = new XPDatabaseConnector(xpDatabaseLogin);
    }

    /**
     * Is triggered when a message is recieved.
     * @param event the event with the message that was received.
     */
    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        try {
            String messageUserId = event.getAuthor().getId();
            UserXP userXP;
            if (!event.getAuthor().isBot()
                    && !this.userIdList.contains(messageUserId)
                    && event.getGuild().getId().equals(Main.getCafGuild().getId())
            ) {
                userXP = connector.selectByUserId(messageUserId);

                if (userXP == null) {
                    connector.insertUserXP(new UserXP(messageUserId));
                }

                userXP = connector.selectByUserId(messageUserId);

                XPListener xpListener = new XPListener(userXP, this.connector);
                Main.getBot().addEventListener(xpListener);
                xpListener.onMessageReceived(event);

                this.userIdList.add(userXP.getUserId());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
