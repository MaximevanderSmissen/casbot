package com.mexmum.casbot.services;

import com.mexmum.casbot.Main;
import com.mexmum.casbot.database.XPDatabaseConnector;
import com.mexmum.casbot.database.objects.UserXP;
import net.dv8tion.jda.core.entities.Role;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Random;
import java.util.TimerTask;
import java.util.function.LongFunction;

/**
 * Listens for incoming messages received by the bot.
 * Gives between 15 and 20 xp if the author of the message has the same Id as this object
 * and it has been more than 20 seconds since the user last received xp.
 */
public class XPListener extends ListenerAdapter {
    private static final int DEFAULT_MIN_XP = 15;
    private static final int MAX_RANDOM_XP = 6;
    private static final long DEFAULT_ANTI_SPAM_DELAY = 20000;
    private static final HashMap<Short, Role> LEVEL_MAP = new HashMap<>();
    @SuppressWarnings("checkstyle:MagicNumber")
    private static final LongFunction<Long> CALCULATE_NEXT_LEVEL_XP = level -> {
        level++;
        return 2 * (level * (level * level + 27 * level + 91)) / 3;
    };

    private final UserXP userXP;
    private final XPDatabaseConnector connector;
    private boolean talkCheck;

    static {
        short level = Short.parseShort("50");
        for (Role role : Main.getRepository().getRankRoles()) {
            LEVEL_MAP.put(level, role);

            level -= Short.parseShort("7");
        }
    }

    /**
     * Default constructor for the XPListener class.
     * @param userXP user that this listener is listening for and it's XP value
     * @param connector the {@link com.mexmum.casbot.database.XPDatabaseConnector DatabaseConnector} which connects to the database
     */
    public XPListener(UserXP userXP, XPDatabaseConnector connector) {
        this.userXP = userXP;
        this.connector = connector;
        this.talkCheck = true;
    }

    /**
     * Getter for the userXP variable.
     * @return the userXP object
     */
    public UserXP getUserXP() {
        return userXP;
    }

    /**
     * Getter for the DatabaseConnector variable.
     * @return the connector
     */
    public XPDatabaseConnector getConnector() {
        return connector;
    }

    /**
     * Sets the value of the talkCheck variable.
     */
    private void setTalkCheckTrue() {
        this.talkCheck = true;
    }

    /**
     * Is triggered when a message is received.
     * @param event the event with the message that was received.
     */
    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        if (!event.getAuthor().isBot()
                && event.getGuild().getId().equals(Main.getCafGuild().getId())
                && event.getAuthor().getId().equals(this.userXP.getUserId())) {
            giveXP(event);
        }
    }

    /**
     * Give the user between 15 and 20 xp and send a message if they reached the next level.
     * @param event the event that initiated the method
     */
    private void giveXP(MessageReceivedEvent event) {
        if (talkCheck) {
            int xpGained = DEFAULT_MIN_XP + new Random().nextInt(MAX_RANDOM_XP);

            this.userXP.setXp(this.userXP.getXp() + xpGained);

            if (this.checkLevel()) {
                TextChannel channel = event.getTextChannel();
                channel.sendMessageFormat("Congratulations Warrior **%s**, you just ascended to level **%d**",
                        Main.getCafGuild().getMemberById(this.userXP.getUserId()).getAsMention(), this.userXP.getLevel()).queue();

                Role role = XPListener.LEVEL_MAP.getOrDefault(this.userXP.getLevel(), null);
                if (role != null) {
                    event.getGuild().getController().addSingleRoleToMember(event.getMember(), role).queue();
                }
            }

            try {
                int result = this.connector.updateXPAmount(userXP);
                if (result == 1) {
                    this.talkCheck = false;
                    Main.getTimer().schedule(new AntiSpamTimerTask(this), DEFAULT_ANTI_SPAM_DELAY);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Check if user has leveled up, and set level if that is the case.
     * @return whether or not the user reached the next level
     */
    private boolean checkLevel() {
        long currentXP = this.userXP.getXp();
        short currentLevel = this.userXP.getLevel();
        long nextLevelXP = CALCULATE_NEXT_LEVEL_XP.apply(currentLevel);

        if (currentXP >= nextLevelXP) {
            this.userXP.setLevel(++currentLevel);
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * TimerTask implementation to set the talkCheck back to true after the specified delay.
     */
    private static class AntiSpamTimerTask extends TimerTask {
        private final XPListener listener;

        /**
         * Default constructor for the AtiSpamTimerTask.
         * @param listener the listener of which the talkCheck value is to be changed
         */
        AntiSpamTimerTask(XPListener listener) {
            this.listener = listener;
        }

        /**
         * Default run method called when the task is to be completed.
         */
        @Override
        public void run() {
            this.listener.setTalkCheckTrue();
        }
    }
}
