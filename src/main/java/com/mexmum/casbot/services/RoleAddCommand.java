package com.mexmum.casbot.services;


import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.Role;

/**
 * Command that adds the given role to the author of the message that initiated the command.
 */
public class RoleAddCommand extends RoleCommand {

    /**
     * Constructor for a command that adds the given role to the member that initiated the command.
     * @param key the key of the command, that the command reacts to.
     * @param message the message of the command, that the command returns upon success.
     * @param role the role this command holds.
     */
    public RoleAddCommand(String key, String message, Role role) {
        super(key, message, role);
    }

    /**
     * Method that executes the command.
     * Sending the message in the same channel as the command was called in.
     * @param call the message that called the command
     */
    @Override
    public void execute(Message call) {
        if (call.getGuild().getSelfMember().canInteract(this.getRole())) {
            Member author = call.getMember();
            Guild cafGuild = call.getGuild();
            cafGuild.getController().addSingleRoleToMember(author, this.getRole())
                    .queue((v) -> super.execute(call));
        }
        else {
            super.execute(call, false);
        }
    }
}
