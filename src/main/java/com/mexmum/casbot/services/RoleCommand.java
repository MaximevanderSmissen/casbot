package com.mexmum.casbot.services;

import net.dv8tion.jda.core.entities.Role;

/**
 * Command that holds a role, parent class for commands that add, and/or remove a role from the author of the message that initiated the command.
 */
public abstract class RoleCommand extends Command {
    private final Role role;

    /**
     * Constructor for a RoleCommand.
     * @param key the key of the command, that the command reacts to.
     * @param message the message of the command, that the command returns upon success.
     * @param role the role this command holds.
     */
    RoleCommand(String key, String message, Role role) {
        super(key, message);
        this.role = role;
    }

    /**
     * Getter for the role variable of this object.
     * @return the role attached to this object.
     */
    public Role getRole() {
        return this.role;
    }
}
