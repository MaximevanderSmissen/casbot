package com.mexmum.casbot.services;

import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.entities.User;

/**
 * Most basic version of a command.
 * Has a String key, the key of the command (used when using the command in Discord)
 * and a String Message, the message that will be returned by the bot.
 */
public class Command {
    private final String key;
    private final String message;
    private static final String FAILED_MESSAGE = "The command failed!";

    /**
     * Constructor for a command.
     * @param key the key of the command (for use in Discord)
     * @param message the message the command returns.
     */
    public Command(String key, String message) {
        this.key = key;
        this.message = message;
    }

    /**
     * Getter for the key of this command.
     * @return the key
     */
    public String getKey() {
        return key;
    }

    /**
     * Getter for the message of this command.
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * Getter for a message when the command failed.
     * @return the String that should be returned when a command failed
     */
    public String getFailedMessage() {
        return Command.FAILED_MESSAGE;
    }

    /**
     * Method that executes the command.
     * Sending the message in the same channel as the command was called in.
     * @param call the message that called the command
     */
    public void execute(Message call) {
        this.execute(call, true);
    }

    /**
     * Method that executes the command, where a previous command might not have been successful.
     * @param call the message that called the command
     * @param commandSuccess whether or not the previous command was successful
     */
    protected void execute(Message call, boolean commandSuccess) {
        TextChannel channel = call.getTextChannel();
        String msg = this.getMessage();
        if (!commandSuccess) {
            msg = this.getFailedMessage();
        }
        if (channel.canTalk()) {
            channel.sendMessage(msg).queue();
        }
        else {
            this.noSendPerms(call, msg);
        }
    }

    /**
     * Run when the bot isn't permitted to talk in the channel a command was send in.
     * @param call the call that initiated the command.
     * The author of this message, or the owner of the guild, will be contacted with a request to contact the servers staff.
     * @param msg the message to put before the no talking announcement
     */
    protected void noSendPerms(Message call, String msg) {
        User author = call.getAuthor();
        if (author.isBot()) {
            author = call.getGuild().getOwner().getUser();
        }
        author.openPrivateChannel().complete().sendMessage(msg
                + "\nI am unable to talk in the channel you send the command in, please contact an admin or mod!").queue();
    }
}
