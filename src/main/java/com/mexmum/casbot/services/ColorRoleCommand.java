package com.mexmum.casbot.services;

import com.mexmum.casbot.Main;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.Role;

import java.util.Collections;
import java.util.List;

/**
 *
 */
public class ColorRoleCommand extends RoleCommand {
    /**
     * Constructor for a ColorRoleCommand.
     * @param key     the key of the command, that the command reacts to.
     * @param message the message of the command, that the command returns upon success.
     * @param role    the role this command holds.
     */
    public ColorRoleCommand(String key, String message, Role role) {
        super(key, message, role);
    }

    /**
     * Method that executes the command.
     * Sending the message in the same channel as the command was called in.
     * @param call the message that called the command
     */
    @Override
    public void execute(Message call) {
        if (call.getGuild().getSelfMember().canInteract(this.getRole())) {
            Member author = call.getMember();
            Guild cafGuild = call.getGuild();
            List<Role> colorRemoveRoles = Main.getRepository().getColorRoles();
            colorRemoveRoles.remove(this.getRole());
            cafGuild.getController().modifyMemberRoles(author, Collections.singleton(this.getRole()), colorRemoveRoles)
                    .queue((v) -> super.execute(call));
        }
        else {
            super.execute(call, false);
        }
    }
}
