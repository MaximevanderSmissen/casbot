package com.mexmum.casbot.services;

import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Role;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Object that holds values like roles of the CAF server.
 */
public class Repository {
    private final Guild guild;
    private final List<Role> colorRoles;
    private final List<Role> clubRoles;
    private final List<Role> rankRoles;
    private final Role adminRole;
    private final Map<String, Command> commandMap;

    /**
     * Instantiates an object, with all the needed lists.
     * @param guild the guild from which the roles come.
     */
    public Repository(Guild guild) {
        List<Role> allGuildRoles = guild.getRoles();
        this.guild = guild;
        this.colorRoles = allGuildRoles.stream().filter(role -> role.getName().contains("Color-")).collect(Collectors.toList());
        this.clubRoles = allGuildRoles.stream().filter(role -> role.getName().contains("Club-")).collect(Collectors.toList());
        this.rankRoles = allGuildRoles.stream().filter(role -> role.getName().contains("\uD835\uDD64")).collect(Collectors.toList());
        this.adminRole = allGuildRoles.stream()
                .filter(role -> role.getName().contains("Celestial Admins")
                && role.hasPermission(Permission.ADMINISTRATOR)).findFirst().orElseGet(null);
        this.commandMap = new HashMap<>();
    }

    /**
     * Adds a command to the command map.
     * @param command the command to add
     */
    public void addCommandToMap(Command command) {
        this.commandMap.put(command.getKey(), command);
    }

    /**
     * Getter for the color roles list.
     * @return the list
     */
    public List<Role> getColorRoles() {
        return colorRoles;
    }

    /**
     * Getter for the club roles list.
     * @return the list
     */
    public List<Role> getClubRoles() {
        return clubRoles;
    }

    /**
     * Getter for the guild.
     * @return the guild
     */
    public Guild getGuild() {
        return guild;
    }

    /**
     * Getter for the rank roles list.
     * @return the list
     */
    public List<Role> getRankRoles() {
        return rankRoles;
    }

    /**
     * Getter for the admin role of the server.
     * @return the role
     */
    public Role getAdminRole() {
        return adminRole;
    }

    /**
     * Gets a command from the object's command map.
     * @param key the key on which to search the command
     * @return the found command, or a default command with an error message
     */
    public Command getCommand(String key) {
        return this.commandMap.getOrDefault(key, new Command(null, "There is no command with this key!"));
    }
}
