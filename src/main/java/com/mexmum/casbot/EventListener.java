package com.mexmum.casbot;

import net.dv8tion.jda.core.events.ShutdownEvent;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;


/**
 * Class designed to handle all the events coming from JDA.
 * Certain events will call a certain method in the {@link com.mexmum.casbot.services services} package.
 */
public class EventListener extends ListenerAdapter {

    /**
     * Is triggered when a message is recieved.
     * @param event the event with the message that was recieved.
     */
    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        String content = event.getMessage().getContentDisplay();
        if (content.startsWith(Main.getPrefix()) && !event.getAuthor().isBot()) {
            CommandHandler.mainCommandHandler(event);
        }
    }

    /**
     * Is triggered when the bot gets the shutdown signal.
     * @param event the event
     */
    @Override
    public void onShutdown(ShutdownEvent event) {
        CommandReadWriter.writeToFile(Main.getCommandFile());
        System.exit(0);
    }
}
