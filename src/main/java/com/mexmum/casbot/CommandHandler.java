package com.mexmum.casbot;

import com.mexmum.casbot.services.ColorRoleCommand;
import com.mexmum.casbot.services.Command;
import com.mexmum.casbot.services.RoleAddCommand;
import com.mexmum.casbot.services.RoleRemoveCommand;
import net.dv8tion.jda.core.entities.Role;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Handles incoming commands and the addition of new ones.
 */
public final class CommandHandler {

    /**
     * Constructor that makes sure this class won't have any objects.
     */
    private CommandHandler() {
        throw new UnsupportedOperationException();
    }

    /**
     * Main method for handling commands.
     * @param event the event with the message containing a command
     */
    static void mainCommandHandler(MessageReceivedEvent event) {
        String content = event.getMessage().getContentRaw();
        content = content.substring(Main.getPrefix().length());
        if (content.startsWith("Shutdown")) {
            Main.getBot().shutdownNow();
        }
        else if (content.startsWith("AddCommand")
                && event.getMember().getRoles().contains(Main.getRepository().getAdminRole())) {
            addCommand(event);
        }
        else {
            String key = content.split(" ")[0];
            Command command = Main.getRepository().getCommand(key);
            command.execute(event.getMessage());
        }
    }

    /**
     * Runs when a command should be added.
     * @param event the event with the command
     */
    private static void addCommand(MessageReceivedEvent event) {
        try {
            String content = event.getMessage().getContentRaw().trim().substring("!AddCommand".length());
            List<String> arguments = Arrays.stream(content.split("``")).map(String::trim).collect(Collectors.toList());
            arguments.removeIf(String::isEmpty);
            String key = argumentGetter(arguments, "key:");
            String message = argumentGetter(arguments, "message:");
            String type = argumentGetter(arguments, "type:");
            Role role = null;
            if (content.contains("role:")) {
                String roleId = argumentGetter(arguments, "role:");
                role = event.getGuild().getRoleById(roleId);
            }
            if (type.contains("role:") && role == null) {
                throw new IllegalArgumentException("Missing role mention in role command!");
            }

            Command command = commandMaker(key, message, type, role);
            CommandReadWriter.addCommand(command);
        }
        catch (IllegalArgumentException e) {
            event.getMessage().getTextChannel().sendMessage(e.getMessage()).queue();
        }
        catch (NullPointerException e) {
            event.getMessage().getTextChannel().sendMessage("One or more arguments were missing!").queue();
            e.printStackTrace();
        }
    }

    /**
     * Gets an argument from the given list, by checking for the specified key.
     * @param arguments the list from which the argument comes
     * @param key the key which indicates the argument
     * @return the argument, minus it's key
     * @throws NullPointerException thrown when the list doesn't contain the key
     */
    private static String argumentGetter(List<String > arguments, final String key) throws NullPointerException {
        String argument = arguments.stream().filter(arg -> arg.startsWith(key)).findFirst().get();
        return argument.substring(key.length());
    }

    /**
     * Converts the given arguments into a command of the given type.
     * @param key the key of the new command
     * @param message the message of the new command
     * @param type the type of the new command
     * @param role the role of the new command, can be null
     * @throws IllegalArgumentException when the type is not supported
     */
    private static Command commandMaker(String key, String message, String type, Role role) throws IllegalArgumentException {
        Command command;
        switch (type) {
            case "Command" :
                command = new Command(key, message);
                break;
            case "RoleAddCommand" :
                command = new RoleAddCommand(key, message, role);
                break;
            case "RoleRemoveCommand" :
                command = new RoleRemoveCommand(key, message, role);
                break;
            case "ColorRoleCommand" :
                command = new ColorRoleCommand(key, message, role);
                break;
            default:
                throw new IllegalArgumentException("No command of this type was found!");
        }

        return command;
    }

    private static void addHelpCommands() {
        String helpMessage = "Use the following commands to see how to use commands for adding colors or clubs!\n"
                + "\n``!HelpCommand`` For help on use of commands in general;\n"
                + "``!HelpColorAdd`` For help with the ``!AddColor{color}`` commands;\n"
                + "``!HelpClubAdd`` For help with the ``!AddClub{club}`` commands;\n"
                + "``!HelpRoleRemove`` For help with the ``!RemoveColor{color}`` and ``!RemoveClub{club}`` commands.\n"
                + "\nIf you need help, you can always ask the CAF Staff!";
        CommandReadWriter.addCommand(new Command("Help", helpMessage));

        String commandHelpMessage = "Sends a message, like these help commands.\n"
                + "\n**Effect:** sends a message in the same channel as you send the command in.\n"
                + "\n**Command:** ``!{Command}`` example: ``!Help``";
        CommandReadWriter.addCommand(new Command("HelpCommand", commandHelpMessage));

        String addColorHelpMessage = "Command for changing your color.\n"
                + "\n**Effect:** Removes all color roles (Roles with a name starting in \"Color-\") and gives you the color you chose.\n"
                + "\n**Command:** ``!AddColor{Color}``, example: ``!AddColorSage``.";
        CommandReadWriter.addCommand(new Command("HelpColorAdd", addColorHelpMessage));

        String addClubHelpMessage = "Command for adding clubs.\n"
                + "\n**Effect:** Adds the club you chose.\n"
                + "\n**Command:** ``!AddClub{Club}``, example ``!AddClubSpoils``.";
        CommandReadWriter.addCommand(new Command("HelpClubAdd", addClubHelpMessage));

        String roleRemoveHelpMessage = "Command for removing colors or clubs\n"
                + "\n**Effect:** Remove the given role. ***Used for both removing color and club roles***\n"
                + "\n**Command:** ``!RemoveColor{Color}`` or ``!RemoveClub{Club}`` examples: ``!RemoveColorSage`` and ``!RemoveClubSpoils``";
        CommandReadWriter.addCommand(new Command("HelpRoleRemove", roleRemoveHelpMessage));
    }
}
