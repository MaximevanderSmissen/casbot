package com.mexmum.casbot.database.objects;

/**
 * Class to make it easier to store new or update existing records in the xp database.
 * Used in the {@link com.mexmum.casbot.database.XPDatabaseConnector XPDatabase}
 */
public final class UserXP {

    private String userId;
    private int xp;
    private short level;

    /**
     * Default constructor for new UserXP object.
     */
    public UserXP() {
        this.userId = null;
        this.xp = 0;
        this.level = 0;
    }

    /**
     * Default constructor for new UserXP object that sets the userId.
     * @param userId the userId
     */
    public UserXP(String userId) {
        this.userId = userId;
        this.xp = 0;
        this.level = 0;
    }

    /**
     * Default constructor for new UserXP object with set userId and xp value.
     * @param userId the userId
     * @param xp the xp value
     * @param level the level
     */
    public UserXP(String userId, int xp, short level) {
        this.userId = userId;
        this.xp = xp;
        this.level = level;
    }

    /**
     * Gets the userId of this object.
     * @return the userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Sets the userId for this object.
     * @param userId the userId
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * Gets the xp value of this object.
     * @return the xp value
     */
    public int getXp() {
        return xp;
    }

    /**
     * Sets the xp value for this object.
     * @param xp the xp value
     */
    public void setXp(int xp) {
        this.xp = xp;
    }

    /**
     * Gets the level value of this object.
     * @return the level value
     */
    public short getLevel() {
        return level;
    }

    /**
     * Sets the level value for this object.
     * @param level the new level value
     */
    public void setLevel(short level) {
        this.level = level;
    }
}
