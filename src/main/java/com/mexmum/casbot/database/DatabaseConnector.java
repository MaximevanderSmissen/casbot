package com.mexmum.casbot.database;

import org.json.simple.JSONObject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Base class for connections to a database.
 */
abstract class DatabaseConnector {

    private final Connection databaseConnection;

    /**
     * Creates an instance of a DatabaseConnector, with a connection to the database at the given URL.
     * @param databaseURL the URL at which the database is located
     * @param jsonDatabaseObject a {@link org.json.simple.JSONObject JSONObject}, used to log in to the database,
     * that contains the user under the key "user" and password under the key "password", can be null for no login data
     * @throws SQLException thrown when the given URL doesn't lead to valid or accessible database
     */
    DatabaseConnector(String databaseURL, JSONObject jsonDatabaseObject) throws SQLException {
        this.databaseConnection = getConnection(databaseURL, jsonDatabaseObject);
    }

    /**
     * Gets the {@link java.sql.Connection Connection} of this object.
     * @return the {@link java.sql.Connection Connection}
     */
    public Connection getDatabaseConnection() {
        return databaseConnection;
    }

    /**
     * Closes the objects connection to the database.
     * @throws SQLException if a database access error occurs or this method is called on a closed connection
     */
    public void closeConnection() throws SQLException {
        this.getDatabaseConnection().close();
    }

    /**
     * Returns a connection to the XPSystem database.
     * @param dbURL URL of the database
     * @param jsonDatabaseLoginObject JSONObject containing the login info, can be null for no login data
     * @throws SQLException thrown when the given URL doesn't lead to valid or accessible database
     * @return the connection
     */
    private static Connection getConnection(String dbURL, JSONObject jsonDatabaseLoginObject) throws SQLException {
        if (jsonDatabaseLoginObject != null) {
            String user = jsonDatabaseLoginObject.get("user").toString();
            String password = jsonDatabaseLoginObject.get("password").toString();

            return DriverManager.getConnection(dbURL, user, password);
        }
        else {
            return DriverManager.getConnection(dbURL);
        }
    }

    /**
     * Executes an update query (insert, update or delete) and returns the number of affected rows after closing the statement.
     * @param statement the to be executed statement
     * @return number of rows affected
     * @throws SQLException if a database access error occurs or this method is called on a closed connection
     */
    static int executeUpdateQuery(PreparedStatement statement) throws SQLException {
        int retValue = statement.executeUpdate();
        statement.close();
        return retValue;
    }

    /**
     * Executes a query that returns a ResultSet.
     * @param statement the to be executed statement
     * @return the ResultSet that the query returned
     * @throws SQLException if a database access error occurs or this method is called on a closed connection
     */
    static ResultSet executeSelectQuery(PreparedStatement statement) throws SQLException {
        ResultSet resultSet = statement.executeQuery();
        return resultSet;
    }
}
