package com.mexmum.casbot.database;

import com.mexmum.casbot.Main;
import com.mexmum.casbot.database.objects.UserXP;
import org.json.simple.JSONObject;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Class that specifically connects to the XPDatabase.
 * Has CRUD methods to interact with the table in the database.
 */
public class XPDatabaseConnector extends DatabaseConnector {

    private static final int MAX_USER_ID_LENGTH = 18;

    /**
     * Creates an instance of a connection to the xp database.
     * @param databaseLogin {@link org.json.simple.JSONObject JSONObject} that holds the login info for the database
     * @throws SQLException thrown when the given URL doesn't lead to valid or accessible database
     */
    public XPDatabaseConnector(JSONObject databaseLogin) throws SQLException {
        super("jdbc:derby:" + Main.getResourceDir() + "database", databaseLogin);
    }

    /**
     * Converts the given object into a valid SQL query and executes it.
     * @param userXP the object to be added
     * @return the number of rows affected
     * @throws SQLException if a database access error occurs or this method is called on a closed connection
     */
    public int insertUserXP(UserXP userXP) throws SQLException {
        if (userXP.getUserId().length() > MAX_USER_ID_LENGTH) {
            throw new IllegalArgumentException("Max length of userId is 18!");
        }
        else {
            PreparedStatement statement =
                    this.getDatabaseConnection().prepareStatement("INSERT INTO USERXP (USERID) VALUES (?)");
            statement.setString(1, userXP.getUserId());

            return XPDatabaseConnector.executeUpdateQuery(statement);
        }
    }

    /**
     * Converts the {@link com.mexmum.casbot.database.objects.UserXP UserXP} objects
     * in the given {@link java.util.List List<{@link UserXP UserXP}>}.
     * @param userXPList the list
     * @return the number of rows added
     * @throws SQLException if a database access error occurs or this method is called on a closed connection
     */
    public int insertUserXPs(List<UserXP> userXPList) throws SQLException {
        String sqlQuery = "INSERT INTO USERXP (USERID) VALUES ";

        for (UserXP userXP : userXPList) {
            if (userXP.getUserId().length() > MAX_USER_ID_LENGTH) {
                throw new IllegalArgumentException("Max length of userId is 18!");
            }
            else {
                String concatString = String.format("'%s',", userXP.getUserId());
                sqlQuery = sqlQuery.concat(concatString);
            }
        }

        return this.getDatabaseConnection().prepareStatement(sqlQuery.substring(0, sqlQuery.length() - 1)).executeUpdate();
    }

    /**
     * Updates the record with the given userId as primary key to the given UserXP object.
     * @param userId the id of the to be updated record
     * @param userXP the new values of the record
     * @return the amount of rows affected
     * @throws SQLException if a database access error occurs or this method is called on a closed connection
     */
    @SuppressWarnings("checkstyle:MagicNumber")
    public int updateUserXP(String userId, UserXP userXP) throws SQLException {
        PreparedStatement statement =
                this.getDatabaseConnection().prepareStatement("UPDATE USERXP SET USERID = ?, XP = ?, LEVEL = ? WHERE USERID = ?");
        statement.setString(1, userXP.getUserId());
        statement.setInt(2, userXP.getXp());
        statement.setShort(3, userXP.getLevel());
        statement.setString(4, userId);

        return XPDatabaseConnector.executeUpdateQuery(statement);
    }

    /**
     * Updates all of the entries contained in the map
     * by using the key-value pair as parameters for {@link XPDatabaseConnector#updateUserXP updateUserXP}.
     * @param updateMap the map
     * @return number of rows affected
     * @throws SQLException if a database access error occurs or this method is called on a closed connection
     */
    public int updateUserXPs(Map<String, UserXP> updateMap) throws SQLException {
        int total = 0;

        for (Map.Entry<String, UserXP> entry : updateMap.entrySet()) {
            String userId = entry.getKey();
            UserXP userXP = entry.getValue();
            total += updateUserXP(userId, userXP);
        }

        return total;
    }

    /**
     * Update the XP column of record with the userId of the given UserXP object.
     * @param userXP the UserXP object with the new XP value
     * @return the number of rows affected
     * @throws SQLException if a database access error occurs or this method is called on a closed connection
     */
    @SuppressWarnings("checkstyle:MagicNumber")
    public int updateXPAmount(UserXP userXP) throws SQLException {
        PreparedStatement statement = this.getDatabaseConnection().prepareStatement("UPDATE USERXP SET XP = ?, LEVEL = ? WHERE USERID = ?");
        statement.setInt(1, userXP.getXp());
        statement.setShort(2, userXP.getLevel());
        statement.setString(3, userXP.getUserId());

        return XPDatabaseConnector.executeUpdateQuery(statement);
    }

    /**
     * Deletes the record belonging to the given userId.
     * @param userId the userId
     * @return the number of rows affected
     * @throws SQLException if a database access error occurs or this method is called on a closed connection
     */
    public int deleteUserXP(String userId) throws SQLException {
        PreparedStatement statement = this.getDatabaseConnection().prepareStatement("DELETE FROM USERXP WHERE USERID = ?");

        statement.setString(1, userId);

        return XPDatabaseConnector.executeUpdateQuery(statement);
    }

    /**
     * Deletes all of the records belonging to userIds in the given list.
     * @param userIdList the list containing userIds
     * @return the number of records affected
     * @throws SQLException if a database access error occurs or this method is called on a closed connection
     */
    public int deleteUserXPs(List<String> userIdList) throws SQLException {
        int total = 0;

        for (String userId : userIdList) {
            total += deleteUserXP(userId);
        }

        return total;
    }

    /**
     * Returns a list of all the {@link com.mexmum.casbot.database.objects.UserXP UserXP} records from the USERXP table.
     * @return the list
     * @throws SQLException if a database access error occurs or this method is called on a closed connection
     */
    public List<UserXP> selectAll() throws SQLException {
        ResultSet resultSet = XPDatabaseConnector.executeSelectQuery(this.getDatabaseConnection().prepareStatement("SELECT * FROM USERXP"));
        List<UserXP> userXPList = new ArrayList<>();

        while (resultSet.next()) {
            userXPList.add(new UserXP(resultSet.getString("USERID"), resultSet.getInt("XP"), resultSet.getShort("LEVEL")));
        }

        resultSet.close();

        return userXPList;
    }

    /**
     * Returns the requested {@link com.mexmum.casbot.database.objects.UserXP UserXP} object, or null.
     * @param userId the id of the requested UserXP object
     * @return the UserXP object
     * @throws SQLException if a database access error occurs or this method is called on a closed connection
     */
    public UserXP selectByUserId(String userId) throws SQLException {
        if (userId.length() > MAX_USER_ID_LENGTH) {
            throw new IllegalArgumentException("Max length of userId is 18!, argument given is \"" + userId + "\"");
        }
        PreparedStatement statement = this.getDatabaseConnection().prepareStatement("SELECT * FROM USERXP WHERE USERID = ? ");
        statement.setString(1, userId);

        ResultSet resultSet = XPDatabaseConnector.executeSelectQuery(statement);

        if (resultSet.next()) {
            UserXP userXP = new UserXP(resultSet.getString("USERID"), resultSet.getInt("XP"), resultSet.getShort("LEVEL"));
            resultSet.close();
            return userXP;
        }
        else {
            resultSet.close();
            return null;
        }
    }
}
