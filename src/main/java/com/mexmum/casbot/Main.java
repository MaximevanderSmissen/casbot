package com.mexmum.casbot;

import com.mexmum.casbot.services.Repository;
import com.mexmum.casbot.services.XPListenerHandler;
import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import net.dv8tion.jda.core.entities.Game;
import net.dv8tion.jda.core.entities.Guild;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.security.auth.login.LoginException;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Timer;

/**
 * Main class.
 */
public final class Main {

    private static final String RESOURCE_DIR = System.getProperty("user.dir").replace("/target", "").replace("\\target", "") + "/resources/";
    private static final String CASBOT_PROPERTIES = RESOURCE_DIR + "CASBotProperties.json";
    private static final String COMMAND_FILE = RESOURCE_DIR + "CommandRepository.json";
    private static final String CAF_ID = "268708038058311683";
    private static final Timer TIMER = new Timer();
    private static JDA bot;
    private static Guild cafGuild;
    private static Repository repository;
    private static String prefix;
    private static JSONObject propertiesObject;

    /**
     * Constructor that makes sure this class won't have any objects.
     */
    private Main() {
        throw new UnsupportedOperationException();
    }

    /**
     * Main method of the program, initiates the bot.
     * @param args the argument with which the program was started
     * @throws LoginException thrown when
     */
    public static void main(String[] args) throws LoginException {
        JSONParser jsonParser = new JSONParser();
        File tokenFile = new File(CASBOT_PROPERTIES);

        try (FileReader reader = new FileReader(tokenFile)) {
            //Read JSON File
            propertiesObject = (JSONObject) jsonParser.parse(reader);
            String token = (String) propertiesObject.get("token");
            prefix = (String) propertiesObject.get("prefix");

            //Build the bot and wait till it's ready.
            JDA bot = new JDABuilder(AccountType.BOT).setGame(Game.of(Game.GameType.DEFAULT, "!Help for help"))
                    .setToken(token).addEventListener(new EventListener()).build().awaitReady();
            setVars(bot);
        }  catch (IOException | ParseException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Gets the file in which the commands are stored.
     * @return the file
     */
    static File getCommandFile() {
        return new File(COMMAND_FILE);
    }

    /**
     * Gets the path to the resource directory for the program.
     * @return the path
     */
    public static String getResourceDir() {
        return RESOURCE_DIR;
    }

    /**
     * Gets the properties object.
     * @return the properties object
     */
    public static JSONObject getPropertiesObject() {
        return propertiesObject;
    }

    /**
     * Gets the JDA instance of the bot.
     * @return the bot
     */
    public static JDA getBot() {
        return bot;
    }

    /**
     * Gets the CAF guild.
     * @return the guild
     */
    public static Guild getCafGuild() {
        return cafGuild;
    }

    /**
     * Gets the repository.
     * @return the repository
     */
    public static Repository getRepository() {
        return repository;
    }

    /**
     * Gets the timer.
     * @return the timer
     */
    public static Timer getTimer() {
        return TIMER;
    }

    /**
     * Sets the repository for this class.
     * @param repository the repository
     */
    public static void setRepository(Repository repository) {
        Main.repository = repository;
    }

    /**
     * Gets the prefix configured for this bot.
     * @return the prefix
     */
    public static String getPrefix() {
        return prefix;
    }

    /**
     * Setup of variables when the bot is online.
     */
    private static void setVars(JDA bot) {
        //Set local variables
        Main.bot = bot;
        cafGuild = bot.getGuildById(CAF_ID);
        try {
            bot.addEventListener(new XPListenerHandler());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        //Read commands from file
        repository = new Repository(cafGuild);
        CommandReadWriter.readFromFile(getCommandFile());
    }
}
